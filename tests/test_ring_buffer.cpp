#include <vector>

#include <gmock/gmock.h>

#include "hlt/ring_buffer.hpp"


using namespace ::testing;


class RingBufferTest : public Test {
public:

    void SetUp() override {

    }
};

TEST_F(RingBufferTest, InsertElements) {
    hlt::RingBuffer<int, 5> buffer;

    for(int i=0; i<5; ++i) {
        buffer.insert(i);
    }

    for(int i=0; i<5; ++i) {
        ASSERT_THAT(buffer.find(i), Eq(true));
    }
}

TEST_F(RingBufferTest, RingBufferOverwritesElementsAfterMaxSizeWasReached) {
    hlt::RingBuffer<int, 5> buffer;

    for(int i=0; i<10; ++i) {
        buffer.insert(i);
    }

    for(int i=1; i<5; ++i) {
        ASSERT_THAT(buffer.find(i), Eq(false));
    }

    for(int i=5; i<10; ++i) {
        ASSERT_THAT(buffer.find(i), Eq(true));
    }
}
