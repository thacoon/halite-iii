#!/usr/bin/env bash

function help {
    echo "usage: sh run_game.sh --2||--4||--rm"
}

set -e

if [ $# -eq  0 ]; then
    help
    exit
fi

if [ "$1" == "--rm" ]; then
    rm replays/*.log
    rm replays/*.hlt
    exit
fi

cmake .
make MyBot


if [ "$1" == "--2" ]; then
    ./halite --replay-directory replays/ -vvv "./MyBot" "benchmark/v0.0.2"
elif [ "$1" == "--4" ]; then
    ./halite --replay-directory replays/ -vvv "./MyBot" "benchmark/v0.0.2" "benchmark/v0.0.2" "benchmark/v0.0.1"
else
    help
    false
fi