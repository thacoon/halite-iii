Benchmark Bots
==============

The benchmark bots downloaded from the [Halite III website](https://halite.io/learn-programming-challenge/downloads) did not worked correctly.
Therefore, the benchmark bots with fixes were downloaded from [fohristiwhirl/halite3_benchmarks](https://github.com/fohristiwhirl/halite3_benchmarks).

Only the path for the logs was changed.