#include <algorithm>
#include <random>
#include <ctime>

#include "hlt/constants.hpp"
#include "hlt/game.hpp"
#include "hlt/log.hpp"
#include "hlt/path.hpp"
#include "hlt/pre_processing.hpp"

using namespace std;

int main(int argc, char* argv[]) {
    hlt::Game game;
    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.

    hlt::PreProcessing pre_processing(&game);
    pre_processing.process();

    std::unordered_map<hlt::EntityId, hlt::Path> ships_and_paths;

    game.ready("v0.0.4");

    hlt::log::log("Successfully created bot! My Player ID is " + to_string(game.my_id));

    for(;;) {
        game.update_frame();
        shared_ptr<hlt::Player> me = game.me;
        unique_ptr<hlt::GameMap>& game_map = game.game_map;

        vector<hlt::Command> command_queue;

        // remove ships that are no longer alive
        for(auto it = ships_and_paths.begin(); it != ships_and_paths.end(); ) {
            if(me->ships.find(it->first) == me->ships.end())
                it = ships_and_paths.erase(it);
            else
                ++it;
        }

        // add ships that were created
        for(auto it = me->ships.begin(); it != me->ships.end(); ) {
            if(ships_and_paths.find(it->second->id) == ships_and_paths.end()) {
                hlt::Path path(it->second->id, game.game_map, game.me);
                ships_and_paths.insert(std::make_pair(it->second->id, path));
            }
            else {
                ++it;
            }
        }

        for(auto& ship_id_path_pair : ships_and_paths) {
            // always zero halite as ships do not move
            hlt::Path* path = &ship_id_path_pair.second;
            path->calculate(game.turn_number);
            command_queue.push_back(path->get_ship()->move(path->get_next_move()));
        }

        if(
            game.turn_number <= pre_processing.m_max_turn_to_buy_ships &&
            me->ships.size() <= pre_processing.m_max_ships &&
            me->halite >= hlt::constants::SHIP_COST &&
            !game_map->at(me->shipyard)->is_occupied())
        {
            command_queue.push_back(me->shipyard->spawn());
        }

        if (!game.end_turn(command_queue)) {
            break;
        }
    }

    return 0;
}
