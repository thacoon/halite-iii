#ifndef MYBOT_RING_BUFFER_HPP
#define MYBOT_RING_BUFFER_HPP

#include <array>

namespace hlt {
    template <typename T, size_t LENGTH>
    class RingBuffer {
    public:
        RingBuffer();

        /**
         * Check whether a obj is stored in the ring buffer.
         * @param obj
         * @return true if the obj was found otherwise false.
         */
        bool find(T obj) const;

        /**
         * Insert a object into the ring buffer.
         * @param obj
         */
        void insert(T obj) const;

    private:
        mutable std::array<T, LENGTH> m_buffer;
        mutable size_t m_index;
    };

    template <typename T, size_t LENGTH>
    RingBuffer<T, LENGTH>::RingBuffer()
    :m_buffer()
    ,m_index(0)
    {

    }

    template <typename T, size_t LENGTH>
    void RingBuffer<T, LENGTH>::insert(T obj) const{
        m_buffer[m_index] = obj;
        m_index = (m_index + 1) % m_buffer.max_size();
    }

    template <typename T, size_t LENGTH>
    bool RingBuffer<T, LENGTH>::find(T obj) const {
        for(T element : m_buffer) {
            if(element == obj)
                return true;
        }

        return false;
    }
}

#endif //MYBOT_RING_BUFFER_HPP
