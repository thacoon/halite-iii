#include "path.hpp"

template <class Q>
void clear_queue(Q& q) {
    q = Q();
}

hlt::Path::Path(hlt::EntityId ship_id, std::unique_ptr<hlt::GameMap>& game_map, std::shared_ptr<hlt::Player> me)
:m_me(std::move(me))
,m_game_map(game_map)
,m_ship_id(ship_id)
{
    m_ship = m_me->ships[m_ship_id];
    m_next_move = hlt::Direction::STILL;
}

void hlt::Path::update(int turn_number) {
    // m_me->ships is cleared on _update so we need to load the ship every time.
    m_ship = m_me->ships[m_ship_id];
    m_last_positions.insert(m_ship->position);

    m_turn_number = turn_number;
}

void hlt::Path::calculate(int turn_number) {
    update(turn_number);

    // Empty member variables used in calculate_explore_path otherwise we get a seg. fault
    clear_queue(m_goals);
    m_came_from.clear();

    if(m_ship->halite < 600 &&
       m_game_map->at(m_ship)->halite > hlt::constants::MAX_HALITE / (10 * std::log(turn_number/10)) &&
       m_ship->position != m_me->shipyard->position) // prevents ships from staying in shipyard
    {
        m_next_move = hlt::Direction::STILL;
        return;
    }

    if(m_ship->halite > 600 && m_ship->position != m_me->shipyard->position)
        calculate_explore_path(&Path::is_shipyard, &Path::get_value_for_returning);
    else
        calculate_explore_path(nullptr, &Path::get_value_for_exploring);

    // Try different paths if the first one is not working because whatever
    for(int i=0; i < 3; ++i) {
        m_next_move = m_game_map->naive_navigate(m_ship, get_next_position());

        if(m_next_move != hlt::Direction::STILL)
            break;
    }

    // Fallback if nothing works to prevent stucking in one location
    if(m_next_move == hlt::Direction::STILL)
        m_next_move = m_game_map->naive_navigate(m_ship, m_me->shipyard->position);
}

void hlt::Path::calculate_explore_path(bool (Path::*is_goal)(hlt::Position), int (Path::*calculate_value)(hlt::Position)) {
    // Keep track of an expanding ring called the frontier with individual values.
    std::priority_queue<std::pair<hlt::Position, int>, std::vector<std::pair<hlt::Position, int>>, hlt::ComparePath> frontier;

    // Keep track of the total movement value from the start location for every position.
    std::unordered_map<hlt::Position, int> value_so_far;
    
    frontier.push(std::make_pair(m_ship->position, (this->*calculate_value)(m_ship->position)));
    m_came_from.insert(std::make_pair(m_ship->position, m_ship->position));
    m_goals.push(std::make_pair(m_ship->position, (this->*calculate_value)(m_ship->position)));
    value_so_far.insert({m_ship->position, (this->*calculate_value)(m_ship->position)});

    int i = 0;
    while(!frontier.empty() && i < 1000) {
        ++i;

        std::pair<hlt::Position, int> current = frontier.top();
        frontier.pop();

        if(is_goal != nullptr) {
            if((this->*is_goal)(current.first)) {
                m_goals.push(std::make_pair(current.first, INTMAX_MAX));
                break;
            }
        }
        // else: no goal so we just explore the game map

        for(hlt::Position next_position : current.first.get_surrounding_cardinals()) {
            int new_value = value_so_far.find(current.first)->second + (this->*calculate_value)(next_position);

            auto next_value_iterator = value_so_far.find(next_position);
            if(next_value_iterator == value_so_far.end() || new_value > next_value_iterator->second) {
                frontier.push(std::make_pair(next_position, new_value));
                m_goals.push(std::make_pair(next_position, new_value));
                m_came_from.insert(std::make_pair(next_position, current.first));
                value_so_far.insert(std::make_pair(next_position, new_value));
            }
        }
    }
}

int hlt::Path::get_value_for_exploring(hlt::Position position) {
    // only care about friendly ships to avoid navigating the same path
    if(m_game_map->at(position)->is_occupied() && m_game_map->at(position)->ship->owner == m_me->id)
        return -1000;

    // distance is more expensive at the start than at the end
    int distance_value = m_game_map->calculate_distance(m_ship->position, position) * 10;
    double distance_factor = -0.002 * m_turn_number + 1; // (0,1), (500,0)

    // halite that can be uptaken in one turn minus the halite that is needed to move after uptake
    int halite_amount = m_game_map->at(position)->halite;
    int halite_value = 0;

    // 2 or more enemy ships result in 200% uptake
    if(get_enemy_ships_nearby(position) >= 2)
        halite_value = int(halite_amount * 0.5 - 0.1 * (halite_amount - halite_amount * 0.5));
    else
        halite_value = int(halite_amount * 0.25 - 0.1 * (halite_amount - halite_amount * 0.25));

    // Skip positions that were traveled in the last few frames
    // Try to prevent ships to toggle between just two positions instead of exploring
    int additional_costs = 0;
    if(m_last_positions.find(position))
        additional_costs = 500;

    return int(halite_value - distance_value * distance_factor - additional_costs);
}

int hlt::Path::get_value_for_returning(hlt::Position position) {
    int distance_to_shipyard = m_game_map->calculate_distance(position, m_me->shipyard->position);

    if(distance_to_shipyard == 0)
        return 1000;

    auto halite_cost = int(0.1 * m_game_map->at(position)->halite);

    return (1000 / distance_to_shipyard) - halite_cost * 2;
}

int hlt::Path::get_enemy_ships_nearby(hlt::Position position) {
    int enemies_nearby = 0;

    for(int i = 0; i <= 4; ++i) {
        for(int j = 0; j <= 4; ++j) {
            int x = position.x + i;
            int y = position.y + j;

            hlt::Position position_to_check = m_game_map->normalize(hlt::Position(x, y));

            if(m_game_map->at(position_to_check)->is_occupied() &&
               m_game_map->at(position_to_check)->ship->owner != m_me->id)
               ++enemies_nearby;
        }
    }

    return enemies_nearby;
}

hlt::Direction hlt::Path::get_next_move() {
    return m_next_move;
}

hlt::Position hlt::Path::get_next_position() {
    hlt::Position current = m_goals.top().first;
    m_goals.pop();

    std::vector<hlt::Position> path;

    if(current == m_ship->position)
        return current;

    while(current != m_ship->position) {
        path.push_back(current);
        current = m_came_from.at(current);
    }

    return path.back();
}

std::shared_ptr<hlt::Ship> hlt::Path::get_ship() {
    return m_ship;
}

bool hlt::Path::is_shipyard(hlt::Position position) {
    return (position == m_me->shipyard->position);
}

