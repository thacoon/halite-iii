#include "pre_processing.hpp"

hlt::PreProcessing::PreProcessing(hlt::Game* game)
:m_game(game)
{

}

void hlt::PreProcessing::calculate_max_ships() {
    //Use linear function with max turn 200 -> 32x32 and 300 -> 64x64
    double max_turn_to_buy_ships = 3.125 * m_game->game_map->width + 100;
    m_max_turn_to_buy_ships = static_cast<int>(max_turn_to_buy_ships);

    // Use linear function with 64x64 -> 58 ships and 32x32 -> 29 ships and all * 2 if 2 players
    double max_ships = 0.9 * m_game->game_map->width * 4/m_game->players.size();
    m_max_ships = static_cast<size_t>(max_ships);
}

void hlt::PreProcessing::process() {
    calculate_max_ships();
}