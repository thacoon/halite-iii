#ifndef MYBOT_PRE_PROCESSING_H
#define MYBOT_PRE_PROCESSING_H

#include <memory>

#include "game.hpp"
#include "log.hpp"

namespace hlt {
    struct PreProcessing {
    public:
        explicit PreProcessing(hlt::Game* game);

        /**
         * Calculates the max number of ships that should be used at the same time.
         */
        void calculate_max_ships();

        /**
         * Run all pre processing calculations.
         */
        void process();

    public:
        std::unique_ptr<hlt::Game> m_game;
        size_t m_max_ships;
        int m_max_turn_to_buy_ships;
    };
}

#endif //MYBOT_PRE_PROCESSING_H
