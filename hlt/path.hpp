#ifndef PATHFINDING_HPP
#define PATHFINDING_HPP

#include <math.h>
#include <memory>
#include <queue>
#include <unordered_map>
#include <utility>

#include "game_map.hpp"
#include "log.hpp"
#include "player.hpp"
#include "position.hpp"
#include "ship.hpp"

namespace hlt {

    class ComparePath {
    public:
        /**
         * Note that the Compare parameter is defined such that it returns true if its first argument comes before its
         * second argument in a weak ordering. But because the priority queue outputs largest elements first, the
         * elements that "come before" are actually output last. That is, the front of the queue contains the "last"
         * element according to the weak ordering imposed by Compare.
         * Ref: https://en.cppreference.com/w/cpp/container/priority_queue
         * @param lhs
         * @param rhs
         * @return
         */
        bool operator() (const std::pair<hlt::Position, int>& lhs, const std::pair<hlt::Position, int>& rhs) const {
            return lhs.second < rhs.second;
        }
    };

    /*
     * Ref. for pathfinding algorithm: https://www.redblobgames.com/pathfinding/a-star/introduction.html
     */
    class Path {
    public:
        /**
         * Constructor
         */
        explicit Path(hlt::EntityId ship_id, std::unique_ptr<hlt::GameMap>& game_map, std::shared_ptr<hlt::Player> me);

        /**
         * Update the ptr to ship as m_me->ships is cleared on _update, so we need to load the ship every time.
         */
        void update(int turn_number);

        /**
         * Calculates the ship's next moves.
         */
        void calculate(int turn_number);

        /**
         * Calculate a path with the most efficient halite uptake possibilities.
         * @details Reference: https://www.redblobgames.com/pathfinding/a-star/introduction.html#dijkstra
         * @param calculate_value is a function ptr needed to calculate the value of a given position, higher value means better for that specific use case
         * @param is_goal is a function ptr needed to check whether the actual position is the goal
         */
        void calculate_explore_path(bool (Path::*is_goal)(hlt::Position), int (Path::*calculate_value)(hlt::Position));

        /**
         * Calculate the values for a given position if exploring the map. Halite uptake should be maximized.
         * @param position
         * @return Higher values mean more halite.
         */
        int get_value_for_exploring(hlt::Position position);

        /**
         * Calculate the values for a given position if returning to shipyard. Halite costs should be minimized.
         * @param position
         * @return Higher values mean less costs.
         */
        int get_value_for_returning(hlt::Position position);

        /**
         * If there are two or more ships belonging to any opponent within a four-cell radius of a ship, the ship is
         * inspired by the competition. An inspired ship collects halite from the sea at the normal rate, but
         * receives an additional 200% bonus.
         * @param position
         * @return The number of enemy ships within a four-cell radius
         */
        int get_enemy_ships_nearby(hlt::Position position);

        /**
         * Attention this will pop the highest rated goal from m_goals!
         * @return The next position the ship should navigate to. This position belongs to the path with the highest value.
         */
         hlt::Position get_next_position();

        /**
         * @return The next Direction move command.
         */
        hlt::Direction get_next_move();

        /**
         * @return The ship that belongs to this calculated path.
         */
        std::shared_ptr<hlt::Ship> get_ship();

        /**
         * @return True if position equals the position of the shipyard.
         */
        bool is_shipyard(hlt::Position position);

    private:
        /// The player.
        std::shared_ptr<hlt::Player> m_me;

        /// The game map.
        std::unique_ptr<hlt::GameMap>& m_game_map;

        /// The ship that the path is calculated for.
        hlt::EntityId m_ship_id;
        std::shared_ptr<hlt::Ship> m_ship;

        /// Keep track of all goals in relation to their specific value
        std::priority_queue<std::pair<hlt::Position, int>, std::vector<std::pair<hlt::Position, int>>, hlt::ComparePath> m_goals;

        /// Keep track of where we came from for every location that’s visited.
        /// First is the next location and second the came from location.
        std::unordered_map<hlt::Position, hlt::Position> m_came_from;

        /// Store the next move.
        hlt::Direction m_next_move;

        /// Save the last positions. Update on every calculation/frame. Ships cannot save this as these are recreated every new turn.
        hlt::RingBuffer<hlt::Position, 5> m_last_positions;

        /// The current turn number, is updated in calculate
        int m_turn_number = 0;
    };

}


#endif //PATHFINDING_HPP
